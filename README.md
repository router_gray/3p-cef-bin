Important: This file is not part of the CEF distribution.
It only serves to outline what is here and remind users
who want to update to different versions of CEF that the 
CEF bundles (bz2 files) are downloaded by build-cmd.sh and
build locally. They may or may not be proxied via a Linden
owned S3 bucket to ensure we have a copy if the current
provider (Spotify) decides to remove the page of automated
builds.

------------------------------------------------------------

The Chromium Embedded Framework (CEF) is a simple framework for
embedding Chromium-based browsers in other applications.

It is Copyright 2008-2014 Marshall A. Greenblatt. 
Portions Copyright (c) 2006-2009 Google Inc. All rights reserved.

To update the version of CEF for a platform or bit-width:

* Visit http://opensource.spotify.com/cefbuilds/index.html and
  download the version you want to import. Use the minimal download
  version.

* Add the new code to the repository in the VENDOR branch as per the 
  Linden Lab Mercurial Vendor Branch Strategy outlined here:
  https://wiki.lindenlab.com/wiki/Mercurial_Vendor_Branches

* Merge with the default branch

* Changes so far to the VENDOR branch are:

    Windows:
    * Settings in CMakelists.txt to change MSVC code generation
      settings from /MT to /MD so that it is compatible with the
      Second Life viewer. (only for Release - the Debug configuration
      is not used)

    Darwin:
    * Settings in CMakelists.txt to remove the CEF sample applications
      from tyhe build since they do not build with the minimal download
      distribution.
    * Settings in cmake/cef_settings.cmake to change the default 
      Standard Library to libc++ (-std=libc++) and turn on C++11 features
      (-std=c++11)
