#!/usr/bin/env bash

cd "$(dirname "$0")"

# turn on verbose debugging output for parabuild logs.
exec 4>&1; export BASH_XTRACEFD=4; set -x

# make errors fatal
set -e
# bleat on references to undefined shell variables
set -u

# Check autobuild is around or fail
if [ -z "$AUTOBUILD" ] ; then
    exit 1
fi
if [ "$OSTYPE" = "cygwin" ] ; then
    export AUTOBUILD="$(cygpath -u $AUTOBUILD)"
fi

top="$(pwd)"
stage="$(pwd)/stage"
mkdir -p "${stage}"

# Load autobuild provided shell functions and variables
source_environment_tempfile="$stage/source_environment.sh"
"$AUTOBUILD" source_environment > "$source_environment_tempfile"
. "$source_environment_tempfile"

# version number to build  (this is what to change to update CEF build)
# it will get propagated into the URLs below
CEF_PACKAGE_VERSION="79.1.38%2Bgecefb59%2Bchromium-79.0.3945.130"

# URLs of the CEF bundle to download
CEF_BUNDLE_URL_WINDOWS32="http://opensource.spotify.com/cefbuilds/cef_binary_"$CEF_PACKAGE_VERSION"_windows32.tar.bz2"
CEF_SYM_BUNDLE_URL_WINDOWS32="http://opensource.spotify.com/cefbuilds/cef_binary_"$CEF_PACKAGE_VERSION"_windows32_release_symbols.tar.bz2"
CEF_BUNDLE_URL_WINDOWS64="http://opensource.spotify.com/cefbuilds/cef_binary_"$CEF_PACKAGE_VERSION"_windows64.tar.bz2"
CEF_SYM_BUNDLE_URL_WINDOWS64="http://opensource.spotify.com/cefbuilds/cef_binary_"$CEF_PACKAGE_VERSION"_windows64_release_symbols.tar.bz2"
CEF_BUNDLE_URL_DARWIN64="http://opensource.spotify.com/cefbuilds/cef_binary_"$CEF_PACKAGE_VERSION"_macosx64.tar.bz2"
CEF_SYM_BUNDLE_URL_DARWIN64="http://opensource.spotify.com/cefbuilds/cef_binary_"$CEF_PACKAGE_VERSION"_macosx64_release_symbols.tar.bz2"
CEF_BUNDLE_URL_LINUX64="http://opensource.spotify.com/cefbuilds/cef_binary_"$CEF_PACKAGE_VERSION"_linux64.tar.bz2"

# file where the CEF bundle will be downloaded to before unpacking etc.
CEF_BUNDLE_DOWNLOAD_FILE_WINDOWS="${top}/stage/windows${AUTOBUILD_ADDRSIZE}.bz2"
CEF_SYM_BUNDLE_DOWNLOAD_FILE_WINDOWS="${top}/stage/windows${AUTOBUILD_ADDRSIZE}sym.bz2"
CEF_BUNDLE_DOWNLOAD_FILE_DARWIN64="${top}/stage/darwin64.bz2"
CEF_SYM_BUNDLE_DOWNLOAD_FILE_DARWIN64="${top}/stage/darwin64sym.bz2"
CEF_BUNDLE_DOWNLOAD_FILE_LINUX64="${top}/stage/linux64.bz2"

# directories where the downloaded, unpacked, modified and ready to build CEF
# bundle will end up and where it will be built by Cmake
CEF_BUNDLE_SRC_DIR_WINDOWS="${top}/stage/windows${AUTOBUILD_ADDRSIZE}"
CEF_SYM_BUNDLE_SRC_DIR_WINDOWS="${top}/stage/windows${AUTOBUILD_ADDRSIZE}sym"
CEF_BUNDLE_SRC_DIR_DARWIN64="${top}/stage/darwin64"
CEF_SYM_BUNDLE_SRC_DIR_DARWIN64="${top}/stage/darwin64sym"
CEF_BUNDLE_SRC_DIR_LINUX64="${top}/stage/linux64"

# used in VERSION.txt but common to all bit-widths and platforms
build=${AUTOBUILD_BUILD_ID:=0}

case "$AUTOBUILD_PLATFORM" in
    windows*)
        # download bundle
        CEF_BUNDLE_URL="CEF_BUNDLE_URL_WINDOWS${AUTOBUILD_ADDRSIZE}"
        CEF_SYM_BUNDLE_URL="CEF_SYM_BUNDLE_URL_WINDOWS${AUTOBUILD_ADDRSIZE}"
        /usr/bin/curl "${!CEF_BUNDLE_URL}" -o "${CEF_BUNDLE_DOWNLOAD_FILE_WINDOWS}"
        /usr/bin/curl "${!CEF_SYM_BUNDLE_URL}" -o "${CEF_SYM_BUNDLE_DOWNLOAD_FILE_WINDOWS}"

        # Create directory for it and untar, stripping off the complex CEF name
        mkdir -p "${CEF_BUNDLE_SRC_DIR_WINDOWS}"
        mkdir -p "${CEF_SYM_BUNDLE_SRC_DIR_WINDOWS}"
        /usr/bin/tar xvfj "${CEF_BUNDLE_DOWNLOAD_FILE_WINDOWS}" -C "${CEF_BUNDLE_SRC_DIR_WINDOWS}" --strip-components=1
        /usr/bin/tar xvfj "${CEF_SYM_BUNDLE_DOWNLOAD_FILE_WINDOWS}" -C "${CEF_SYM_BUNDLE_SRC_DIR_WINDOWS}" --strip-components=1

        # create solution file cef.sln in build folder
        cd "${CEF_BUNDLE_SRC_DIR_WINDOWS}"
        rm -rf build
        mkdir -p build
        cd build
        cmake -G "$AUTOBUILD_WIN_CMAKE_GEN" -A${AUTOBUILD_WIN_VSPLATFORM} -DCEF_RUNTIME_LIBRARY_FLAG="/MD" -DCEF_DEBUG_INFO_FLAG="/Z7" ..

        # build release version of wrapper only
        cmake --build . --config Release --target "libcef_dll_wrapper"

        # create folders to stage files in
        mkdir -p "$stage/bin/release"
        mkdir -p "$stage/bin/release/swiftshader"
        mkdir -p "$stage/include/cef/include"
        mkdir -p "$stage/lib/release"
        mkdir -p "$stage/resources"
        mkdir -p "$stage/LICENSES"

        # binary files
        cp "${CEF_BUNDLE_SRC_DIR_WINDOWS}/release/chrome_elf.dll" "$stage/bin/release/"
        cp "${CEF_BUNDLE_SRC_DIR_WINDOWS}/release/d3dcompiler_47.dll" "$stage/bin/release/"
        cp "${CEF_BUNDLE_SRC_DIR_WINDOWS}/release/libcef.dll" "$stage/bin/release/"
        cp "${CEF_BUNDLE_SRC_DIR_WINDOWS}/release/libEGL.dll" "$stage/bin/release/"
        cp "${CEF_BUNDLE_SRC_DIR_WINDOWS}/release/libGLESv2.dll" "$stage/bin/release/"
        cp "${CEF_BUNDLE_SRC_DIR_WINDOWS}/release/natives_blob.bin" "$stage/bin/release/"
        cp "${CEF_BUNDLE_SRC_DIR_WINDOWS}/release/snapshot_blob.bin" "$stage/bin/release/"
        cp "${CEF_BUNDLE_SRC_DIR_WINDOWS}/release/v8_context_snapshot.bin" "$stage/bin/release/"
        cp "${CEF_BUNDLE_SRC_DIR_WINDOWS}/release/swiftshader/libEGL.dll" "$stage/bin/release/swiftshader/"
        cp "${CEF_BUNDLE_SRC_DIR_WINDOWS}/release/swiftshader/libGLESv2.dll" "$stage/bin/release/swiftshader/"

        # include files
        cp -r "${CEF_BUNDLE_SRC_DIR_WINDOWS}/include/." "$stage/include/cef/include/"

        # resource files
        cp -r "${CEF_BUNDLE_SRC_DIR_WINDOWS}/Resources/" "$stage/"

        # library files
        cp "${CEF_BUNDLE_SRC_DIR_WINDOWS}/release/cef_sandbox.lib" "$stage/lib/release/"
        cp "${CEF_BUNDLE_SRC_DIR_WINDOWS}/release/libcef.lib" "$stage/lib/release/"
        cp "${CEF_BUNDLE_SRC_DIR_WINDOWS}/build/libcef_dll_wrapper/Release/libcef_dll_wrapper.lib" "$stage/lib/release/"

        # license file
        cp "${CEF_BUNDLE_SRC_DIR_WINDOWS}/LICENSE.txt" "$stage/LICENSES/cef.txt"

        # pdb files
        cp "${CEF_SYM_BUNDLE_SRC_DIR_WINDOWS}/libcef.dll.pdb" "$stage/bin/release/"

        # populate version_file (after header files are copied to a well specified place that version.cpp can access)
        cl \
            /Fo"$(cygpath -w "$stage/version.obj")" \
            /Fe"$(cygpath -w "$stage/version.exe")" \
            /I "$(cygpath -w "$stage/include/cef/include")"  \
            /I "$(cygpath -w "$stage/include/cef")"  \
            /D "AUTOBUILD_BUILD=${build}" \
            "$(cygpath -w "$top/version.cpp")"
        "$stage/version.exe" > "$stage/version.txt"
        rm "$stage"/version.{obj,exe}

    ;;

    darwin*)
        # download bundle
        CEF_BUNDLE_URL="CEF_BUNDLE_URL_DARWIN64"
        CEF_SYM_BUNDLE_URL="CEF_SYM_BUNDLE_URL_DARWIN64"
        curl "${!CEF_BUNDLE_URL}" -o "${CEF_BUNDLE_DOWNLOAD_FILE_DARWIN64}"
        curl "${!CEF_SYM_BUNDLE_URL}" -o "${CEF_SYM_BUNDLE_DOWNLOAD_FILE_DARWIN64}"

        # Create directory for it and untar, stripping off the complex CEF name
        mkdir -p "${CEF_BUNDLE_SRC_DIR_DARWIN64}"
        mkdir -p "${CEF_SYM_BUNDLE_SRC_DIR_DARWIN64}"
        tar xvfj "${CEF_BUNDLE_DOWNLOAD_FILE_DARWIN64}" -C "${CEF_BUNDLE_SRC_DIR_DARWIN64}" --strip-components=1
        tar xvfj "${CEF_SYM_BUNDLE_DOWNLOAD_FILE_DARWIN64}" -C "${CEF_SYM_BUNDLE_SRC_DIR_DARWIN64}" --strip-components=1

        cd "${CEF_BUNDLE_SRC_DIR_DARWIN64}"

        BUILD_FOLDER="build"
        rm -rf "${BUILD_FOLDER}"
        mkdir -p "${BUILD_FOLDER}"
        pushd "${BUILD_FOLDER}"
            cmake -G "Xcode" -DPROJECT_ARCH="x86_64" .. -DCMAKE_OSX_SYSROOT="macosx10.15"
            cmake --build . --config Release --target libcef_dll_wrapper

            # create folders to stage files in
            mkdir -p "$stage/bin/release"
            mkdir -p "$stage/include/cef/include"
            mkdir -p "$stage/lib/release"
            mkdir -p "$stage/LICENSES"

            # include files
            cp -a "${CEF_BUNDLE_SRC_DIR_DARWIN64}/include/." "$stage/include/cef/include/"

            # library file
            cp -a "${CEF_BUNDLE_SRC_DIR_DARWIN64}/${BUILD_FOLDER}/libcef_dll_wrapper/Release/libcef_dll_wrapper.a" "$stage/lib/release/"

            # framework
            cp -a "${CEF_BUNDLE_SRC_DIR_DARWIN64}/Release/Chromium Embedded Framework.framework" "$stage/bin/release/"
            cp -a "${CEF_BUNDLE_SRC_DIR_DARWIN64}/Release/cef_sandbox.a" "$stage/lib/release/"

            # include files
            cp -a "${CEF_BUNDLE_SRC_DIR_DARWIN64}/include/." "$stage/include/cef/include/"

            # license file
            cp "${CEF_BUNDLE_SRC_DIR_DARWIN64}/LICENSE.txt" "$stage/LICENSES/cef.txt"

            # pdb files
            cp -a "${CEF_SYM_BUNDLE_SRC_DIR_DARWIN64}/Chromium Embedded Framework.dSYM" "$stage/bin/release/"

            # populate version_file
            clang++ -I "$stage/include/cef/include" \
                -I "$stage/include/cef" \
                -D "AUTOBUILD_BUILD=${build}" \
                -o "$stage/version" "$top/version.cpp"
            "$stage/version" > "$stage/VERSION.txt"
            rm "$stage/version"
        popd
    ;;

    linux*)
        # download bundle
        CEF_BUNDLE_URL="${CEF_BUNDLE_URL_LINUX64}"

        file="${CEF_BUNDLE_DOWNLOAD_FILE_LINUX64}"
        if [ ! -e "$file" ]; then
            curl "${CEF_BUNDLE_URL}" -o "${CEF_BUNDLE_DOWNLOAD_FILE_LINUX64}"
        else 
            echo "CEF bundle exists, skipping download. To force a fresh fetch, delete ${CEF_BUNDLE_DOWNLOAD_FILE_LINUX64}"
        fi 

        # Create directory for it and untar, stripping off the complex CEF name
        mkdir -p "${CEF_BUNDLE_SRC_DIR_LINUX64}"
        tar xvfj "${CEF_BUNDLE_DOWNLOAD_FILE_LINUX64}" -C "${CEF_BUNDLE_SRC_DIR_LINUX64}" --strip-components=1

        cd "${CEF_BUNDLE_SRC_DIR_LINUX64}"
        rm -rf build
        mkdir -p build
        cd build

        cmake .. -DCMAKE_C_COMPILER=gcc-4.9 -DCMAKE_CXX_COMPILER=g++-4.9 -DCMAKE_CXX_FLAGS=-m${AUTOBUILD_ADDRSIZE} || true
        make -j4 libcef_dll_wrapper
        cd ../..

        mkdir -p "${stage}/include/cef/include"
        mkdir -p "${stage}/lib/release"
        mkdir -p "${stage}/resources"
        mkdir -p "${stage}/LICENSES"

        # include files
        cp -R ${CEF_BUNDLE_SRC_DIR_LINUX64}/include/* ${stage}/include/cef/include/

        # library file
        cp ${CEF_BUNDLE_SRC_DIR_LINUX64}/build/libcef_dll_wrapper/libcef_dll_wrapper.a ${stage}/lib/release/

        # framework
        cp -R ${CEF_BUNDLE_SRC_DIR_LINUX64}/Release/* ${stage}/lib/release/
        cp -R ${CEF_BUNDLE_SRC_DIR_LINUX64}/Resources/* ${stage}/resources/

        cp "${CEF_BUNDLE_SRC_DIR_LINUX64}/LICENSE.txt" "$stage/LICENSES/cef.txt"

        # populate version_file
        g++ -I "$stage/include/cef/include" \
            -I "$stage/include/cef" \
            -D "AUTOBUILD_BUILD=${build}" \
            -o "$stage/version" "$top/version.cpp"
        "$stage/version" > "$stage/VERSION.txt"
        rm "$stage/version"
    ;;

esac
